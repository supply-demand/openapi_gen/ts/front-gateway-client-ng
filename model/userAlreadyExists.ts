/**
 * Front gateway
 * Front gateway
 *
 * The version of the OpenAPI document: 0.0.1
 * 
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */


export interface UserAlreadyExists { 
    kind?: UserAlreadyExists.KindEnum;
}
export namespace UserAlreadyExists {
    export type KindEnum = 'userAlreadyExists';
    export const KindEnum = {
        UserAlreadyExists: 'userAlreadyExists' as KindEnum
    };
}


